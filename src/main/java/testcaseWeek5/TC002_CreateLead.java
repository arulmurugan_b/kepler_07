package testcaseWeek5;


import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethodsNew.ProjectMethods;

public class TC002_CreateLead extends ProjectMethods{
	
	@BeforeTest
	public void setData1() {
		testCaseName = "TC002_CreateLead";
		testCaseDesc = "Create a new lead";
		category = "smoke";
		author = "arul";
	}
	
	
	@Test(invocationCount=2)
	
	public void cLead() {
	
		WebElement eleCreateLead = locateElement("linkText", "Create Lead");
		click(eleCreateLead);
    	WebElement eleCompName = locateElement("id", "createLeadForm_companyName");
		type(eleCompName, "Solartis Technology");
		WebElement eleFirstName = locateElement("id","createLeadForm_firstName");
		type(eleFirstName, "Arul");
		WebElement eleLastName = locateElement("id","createLeadForm_lastName");
		type(eleLastName, "Murugan");
		
		//using visibleText
		WebElement eleDropDown = locateElement("id","createLeadForm_dataSourceId");
		selectDropDownUsingText(eleDropDown,"Direct Mail" );
		
		//using index
		WebElement eleDropDown1 = locateElement("id","createLeadForm_marketingCampaignId");
		
		selectDropDownUsingIndex(eleDropDown1, 1 );
		
		WebElement subMit = locateElement("class","smallSubmit");
		click(subMit);
		
		}
	
}
