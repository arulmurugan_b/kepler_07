package testcaseWeek5;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethodsNew.ProjectMethods;

public class TC003_MergeLead extends ProjectMethods{
	@BeforeTest
	public void setData1()
	{
		testCaseName="TC003_MergeLead";
		testCaseDesc="Merge the Lead";
		category="smoke";
		author="arul";
	}
	@Test(priority=2,dependsOnMethods= {"testcaseWeek5.TC002_CreateLead.cLead"})
	public void mergeLead() throws InterruptedException
	{
	click(locateElement("linkText","Leads"));
	click(locateElement("linkText","Merge Leads"));
	click(locateElement("xpath","//input[@id='partyIdFrom']/following-sibling::a/img"));
	//Thread.sleep(7000);
	switchToWindow(1);
	type(locateElement("name", "id"), "101");
	click(locateElement("xpath","//button[text()='Find Leads']"));
	Thread.sleep(7000);
	String fromId = getText(locateElement("xpath","//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']//a"));
	clickWithNoSnap(locateElement("xpath","//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']//a"));
	
	switchToWindow(0);
	click(locateElement("xpath","//input[@id='partyIdTo']/following-sibling::a/img"));
	Thread.sleep(7000);
	switchToWindow(1);
	type(locateElement("name", "id"), "111");
	click(locateElement("xpath","//button[text()='Find Leads']"));
	Thread.sleep(7000);
	clickWithNoSnap(locateElement("xpath","//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']//a"));
	switchToWindow(0);
	clickWithNoSnap(locateElement("linkText","Merge"));
	acceptAlert();
	click(locateElement("linkText","Find Leads"));
	type(locateElement("name", "id"), fromId);
	click(locateElement("xpath","//button[text()='Find Leads']"));
	Thread.sleep(7000);
	verifyExactText(locateElement("xpath","//div[@class='x-paging-info']"), "No records to display");
	closeBrowser();	
	}
}
