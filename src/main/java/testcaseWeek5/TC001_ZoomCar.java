package testcaseWeek5;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TC001_ZoomCar{
	
	public static void main(String[] args) throws InterruptedException {
		//System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
		//FirefoxDriver driver = new FirefoxDriver();
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		//to maximize
		driver.manage().window().maximize();
		//set timeout
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//to enter url
		driver.get("https://www.zoomcar.com/chennai/");
		driver.findElementByClassName("search").click();
		driver.findElementByXPath("(//div[@class='items'])[2]").click();
		driver.findElementByClassName("proceed").click();
		/*// Get the current date
		Date date = new Date();
// Get only the date (and not month, year, time etc)
		DateFormat sdateformat = new SimpleDateFormat("dd"); 
// Get today's date
		String today = sdateformat.format(date);
// Convert to integer and add 1 to it
		int tomorrow = Integer.parseInt(today)+1;
// Print tomorrow's date
		System.out.println(tomorrow);
		*/
		driver.findElementByXPath("(//div[@class='day'])[1]").click();
		driver.findElementByClassName("proceed").click();
		driver.findElementByXPath("(//div[@class='day'])[1]").click();
		driver.findElementByClassName("proceed").click();
		Thread.sleep(5000);
		
		driver.findElementByXPath("//div[text()=' Price: High to Low ']").click();
		//driver.findElementByXPath("(//div[text()='WITHOUT FUEL']").click();
		List<WebElement> findElemt = driver.findElementsByXPath("//div[@class='price']");
		
		List<Integer> ls = new ArrayList<>();
		
		for (WebElement str : findElemt) {

			String allStr = str.getText().replaceAll("\\D", "");	
			ls.add(Integer.parseInt(allStr));
			
		}
		int listSize = ls.size();
		System.out.println(listSize);
		Integer max = Collections.max(ls);
		System.out.println(max);
		String text = driver.findElementByXPath("//div[contains(text(),"+max+")]/preceding::h3[1]").getText();
		System.out.println(text);
		driver.findElementByXPath("//div[contains(text(),"+max+")]/following::button").click();
		//driver.close();
	}
	
}
