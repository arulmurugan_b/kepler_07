package week6.day2;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class CreateLead{
	public static Object readExcel(String fileName) throws IOException {
		
		//get workbook/xlsx 
		XSSFWorkbook work = new XSSFWorkbook("./data/"+fileName+".xlsx");
		//get sheet
		XSSFSheet sheet = work.getSheetAt(0);
		//get last row in a sheet
		int rowNum = sheet.getLastRowNum();
		System.out.println("Row count = "+rowNum);
		//get last column in a sheet
		int colNum = sheet.getRow(0).getLastCellNum();
		System.out.println("Column count = "+colNum);
		
		Object[][] data = new Object[rowNum][colNum];
		
		for(int i =1;i<=rowNum;i++) 
		{
		XSSFRow row = sheet.getRow(i);
		for(int j=0;j<colNum;j++)
		{
		XSSFCell cell = row.getCell(j);
		data[i-1][j] = cell.getStringCellValue();
				
		}
		}
		return data;
			
	}
	
	
	}
	
	
	/*	public void cLead(String cName, String fName, String lName) {
	
		WebElement eleCreateLead = locateElement("linkText", "Create Lead");
		click(eleCreateLead);
    	WebElement eleCompName = locateElement("id", "createLeadForm_companyName");
		type(eleCompName, cName);
		WebElement eleFirstName = locateElement("id","createLeadForm_firstName");
		type(eleFirstName, fName);
		WebElement eleLastName = locateElement("id","createLeadForm_lastName");
		type(eleLastName, lName);
		
		//using visibleText
		WebElement eleDropDown = locateElement("id","createLeadForm_dataSourceId");
		selectDropDownUsingText(eleDropDown,"Direct Mail" );
		
		//using index
		WebElement eleDropDown1 = locateElement("id","createLeadForm_marketingCampaignId");
		
		selectDropDownUsingIndex(eleDropDown1, 1 );
		
		WebElement subMit = locateElement("class","smallSubmit");
		click(subMit);
		
		}
	*/