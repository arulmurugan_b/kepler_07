package week6.day2;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethodsNew.ProjectMethods;

public class Test_CreateLead extends ProjectMethods {

	// @BeforeTest(groups= {"smoke"})
	@BeforeTest
	public void setData1() {
		testCaseName = "TC002_CreateLead";
		testCaseDesc = "Create a new lead";
		category = "smoke";
		author = "arul";
		excelfile = "testXL";
	}

	// @Test(groups= {"smoke"})
	@Test(dataProvider = "getData")
	public void cLead(String cName, String fName, String lName) {

		WebElement eleCreateLead = locateElement("linkText", "Create Lead");
		click(eleCreateLead);
		WebElement eleCompName = locateElement("id", "createLeadForm_companyName");
		type(eleCompName, cName);
		WebElement eleFirstName = locateElement("id", "createLeadForm_firstName");
		type(eleFirstName, fName);
		WebElement eleLastName = locateElement("id", "createLeadForm_lastName");
		type(eleLastName, lName);

		// using visibleText
		WebElement eleDropDown = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingText(eleDropDown, "Direct Mail");

		// using index
		WebElement eleDropDown1 = locateElement("id", "createLeadForm_marketingCampaignId");

		selectDropDownUsingIndex(eleDropDown1, 1);

		WebElement subMit = locateElement("class", "smallSubmit");
		click(subMit);

	}

}
