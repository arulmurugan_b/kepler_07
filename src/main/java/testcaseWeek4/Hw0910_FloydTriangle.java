package testcaseWeek4;

import java.util.Scanner;

public class Hw0910_FloydTriangle {
	
		public static void main(String[] args) {
			
			int num=1;
			Scanner scan=new Scanner(System.in);
			System.out.println("Enter the number of rows:");
			int n=scan.nextInt();
			for(int i=1;i<=n;i++)
			{
				for (int j=1;j<=i;j++)
				{
					System.out.print(num + " ");
					num++;
				}
				System.out.println();
			}
		}

	}
