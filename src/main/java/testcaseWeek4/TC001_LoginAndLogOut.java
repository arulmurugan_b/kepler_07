package testcaseWeek4;


import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import wdMethodsNew.SeMethods;

public class TC001_LoginAndLogOut extends SeMethods{
	
	@Test
	public void login() throws InterruptedException {
		
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id","password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		
		/*driver.findElementBylinkText("CRM/SFA").click();
		driver.findElementBylinkText("Create Lead").click();*/
		WebElement crmSfa = locateElement("linkText","CRM/SFA");
		click(crmSfa);
		WebElement createLead = locateElement("linkText","Create Lead");
		click(createLead);
				
/*		//enter companyname
		driver.findElementById("createLeadForm_companyName").sendKeys("Solartis Tech");*/

		WebElement eleCompName = locateElement("id", "createLeadForm_companyName");
		type(eleCompName, "Solartis Technology");
		WebElement eleFirstName = locateElement("id","createLeadForm_firstName");
		type(eleFirstName, "Arul");
		WebElement eleLastName = locateElement("id","createLeadForm_lastName");
		type(eleLastName, "Murugan");
		
		//using visibleText
		WebElement eleDropDown = locateElement("id","createLeadForm_dataSourceId");
		selectDropDownUsingText(eleDropDown,"Direct Mail" );
		
		//using index
		WebElement eleDropDown1 = locateElement("id","createLeadForm_marketingCampaignId");
		
		selectDropDownUsingIndex(eleDropDown1, 1 );
		
		//using selectByValue
		WebElement eleDropDown2 = locateElement("id","createLeadForm_ownershipEnumId");
		selectDropDownUsingValue(eleDropDown2, "OWN_PROPRIETOR");
		
		
		//verifyTitle(expectedTitle)

		//click the save button
		WebElement subMit = locateElement("class","smallSubmit");
		click(subMit);
		
		WebElement mergeLead = locateElement("linkText","Merge Leads");
		click(mergeLead);
		
		WebElement fromMergeLead = locateElement("xpath","(//img[contains(@src, 'images/fieldlookup.gif')])[1]");
		click(fromMergeLead);
		
		switchToWindow(1);
		
		WebElement fromMergeLead1 = locateElement("xpath","(//div[contains(@class,\"x-grid3-cell-inner x-grid3-col-partyId\")])[1]");
		click(fromMergeLead1);
		
		switchToWindow(1);
		type(locateElement("name", "id"), "10");
		click(locateElement("xpath","//button[text()='Find Leads']"));
		Thread.sleep(7000);
		String fromId = getText(locateElement("xpath","//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']//a"));
		clickWithNoSnap(locateElement("xpath","//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']//a"));
		switchToWindow(0);
		click(locateElement("xpath","//input[@id='partyIdTo']/following-sibling::a/img"));
		Thread.sleep(7000);
		switchToWindow(1);
		type(locateElement("name", "id"), "11");
		click(locateElement("xpath","//button[text()='Find Leads']"));
		Thread.sleep(7000);
		clickWithNoSnap(locateElement("xpath","//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']//a"));
		switchToWindow(0);
		clickWithNoSnap(locateElement("linkText","Merge"));
		acceptAlert();
		click(locateElement("linkText","Find Leads"));
		type(locateElement("name", "id"), fromId);
		click(locateElement("xpath","//button[text()='Find Leads']"));
		Thread.sleep(7000);
		verifyExactText(locateElement("xpath","//div[@class='x-paging-info']"), "No records to display");
		
		
		click(locateElement("linkText","Leads"));
		click(locateElement("linkText","Find Leads"));
		type(locateElement("xpath", "(//input[@name='firstName'])[3]"), "Babu");
		click(locateElement("xpath","//button[text()='Find Leads']"));
		//Thread.sleep(7000);
		click(locateElement("xpath","//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']//a"));
		verifyTitle("View Leads");
		click(locateElement("linkText","Edit"));
		locateElement("id", "updateLeadForm_companyName").clear();
		type(locateElement("id", "updateLeadForm_companyName"),"FreshWork");
		click(locateElement("xpath","//input[@type='submit']"));
		verifyExactText(locateElement("id", "viewLead_companyName_sp"), "FreshWork");
		
		click(locateElement("linkText","Leads"));
		click(locateElement("linkText","Find Leads"));
		click(locateElement("xpath","//span[text()='Phone']"));
		locateElement("name","phoneCountryCode").clear();;
		type(locateElement("name","phoneCountryCode"), "2");
		type(locateElement("name","phoneNumber"), "9003790041");
		click(locateElement("xpath","//button[text()='Find Leads']"));
		Thread.sleep(7000);
		String leadId = getText(locateElement("xpath","//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']//a"));
		click(locateElement("xpath","//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']//a"));
		click(locateElement("linkText","Delete"));
		click(locateElement("linkText","Find Leads"));
		type(locateElement("name", "id"), leadId);
		click(locateElement("xpath","//button[text()='Find Leads']"));
		Thread.sleep(7000);
		verifyExactText(locateElement("xpath","//div[@class='x-paging-info']"), "No records to display");

		click(locateElement("linkText","Leads"));
		click(locateElement("linkText","Find Leads"));
		click(locateElement("xpath","//span[text()='Email']"));
		type(locateElement("name","emailAddress"), "testArul46@gmail.com");
		click(locateElement("xpath","//button[text()='Find Leads']"));
		Thread.sleep(7000);
		String leadName = getText(locateElement("xpath","//td[@class='x-grid3-col x-grid3-cell x-grid3-td-firstName ']//a"));
		click(locateElement("xpath","//td[@class='x-grid3-col x-grid3-cell x-grid3-td-firstName ']//a"));
		click(locateElement("linkText","Duplicate Lead"));
		verifyTitle("Duplicate Lead");
		click(locateElement("xpath","//input[@type='submit']"));
		verifyExactText(locateElement("id","viewLead_firstName_sp"), leadName);
		
//		WebElement eleLogout = locateElement("class", "decorativeSubmit");
	//	click(eleLogout);
	}
	
}







