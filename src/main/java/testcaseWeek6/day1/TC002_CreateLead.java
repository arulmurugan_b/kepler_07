package testcaseWeek6.day1;


import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethodsNew.ProjectMethods;

public class TC002_CreateLead extends ProjectMethods{
	
	//@BeforeTest(groups= {"smoke"})
	@BeforeTest
	public void setData1() {
		testCaseName = "TC002_CreateLead";
		testCaseDesc = "Create a new lead";
		category = "smoke";
		author = "arul";
	}
	
	
	//@Test(groups= {"smoke"})
	@Test(dataProvider="positive")
	public void cLead(String cName, String fName, String lName) {
	
		WebElement eleCreateLead = locateElement("linkText", "Create Lead");
		click(eleCreateLead);
    	WebElement eleCompName = locateElement("id", "createLeadForm_companyName");
		type(eleCompName, cName);
		WebElement eleFirstName = locateElement("id","createLeadForm_firstName");
		type(eleFirstName, fName);
		WebElement eleLastName = locateElement("id","createLeadForm_lastName");
		type(eleLastName, lName);
		
		//using visibleText
		WebElement eleDropDown = locateElement("id","createLeadForm_dataSourceId");
		selectDropDownUsingText(eleDropDown,"Direct Mail" );
		
		//using index
		WebElement eleDropDown1 = locateElement("id","createLeadForm_marketingCampaignId");
		
		selectDropDownUsingIndex(eleDropDown1, 1 );
		
		WebElement subMit = locateElement("class","smallSubmit");
		click(subMit);
		
		}
	@DataProvider(name="positive",indices= {0,2})
	public Object[][] datafetch() {
		Object[][] data = new Object[4][3];
		data[0][0]="solartis";
		data[0][1]="arul";
		data[0][2]="murugan";
		
		data[1][0]="teliant";
		data[1][1]="rajesh";
		data[1][2]="waran";
		
		data[2][0] = "testLeaf";
		data[2][1]  = "Babu";
		data[2][2]  = "M";
		
		data[3][0] = "tuna";
		data[3][1]  = "sethu";
		data[3][2] = "J";
		
		return data;
		
	}
	@DataProvider(name="negative")
	public void dataNegfetch() {
		
	}
	
}
