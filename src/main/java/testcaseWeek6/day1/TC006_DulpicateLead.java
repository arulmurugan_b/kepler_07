package testcaseWeek6.day1;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethodsNew.ProjectMethods;

public class TC006_DulpicateLead extends ProjectMethods{
	
	@BeforeTest(groups= {"regression"},dependsOnGroups= {"smoke"})
	public void setData1() {
		testCaseName = "TC002_CreateLead";
		testCaseDesc = "Create a new lead";
		category = "smoke";
		author = "arul";
	}
	@Test(groups= {"regression"},dependsOnGroups= {"smoke"})
	public void duplicate() throws InterruptedException {
		click(locateElement("linkText","Leads"));
		click(locateElement("linkText","Find Leads"));
		click(locateElement("xpath","//span[text()='Email']"));
		type(locateElement("name","emailAddress"), "testArul46@gmail.com");
		click(locateElement("xpath","//button[text()='Find Leads']"));
		Thread.sleep(7000);
		String leadName = getText(locateElement("xpath","//td[@class='x-grid3-col x-grid3-cell x-grid3-td-firstName ']//a"));
		click(locateElement("xpath","//td[@class='x-grid3-col x-grid3-cell x-grid3-td-firstName ']//a"));
		click(locateElement("linkText","Duplicate Lead"));
		verifyTitle("Duplicate Lead");
		click(locateElement("xpath","//input[@type='submit']"));
		verifyExactText(locateElement("id","viewLead_firstName_sp"), leadName);
		closeBrowser();	
	}

}
