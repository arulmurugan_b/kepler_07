package testcaseWeek6.day1;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethodsNew.ProjectMethods;

public class TC005_DeleteLead extends ProjectMethods{
	
	@BeforeTest(groups= {"sanity"})
	public void setData1() {
		testCaseName = "TC005_DeleteLead";
		testCaseDesc = "Delete a lead";
		category = "smoke";
		author = "arul";
	}
	@Test(groups= {"sanity"})
	public void deleteLead() throws InterruptedException{
		click(locateElement("linkText","Leads"));
		click(locateElement("linkText","Find Leads"));
		click(locateElement("xpath","//span[text()='Phone']"));
		locateElement("name","phoneCountryCode").clear();;
		type(locateElement("name","phoneCountryCode"), "11223");
		type(locateElement("name","phoneNumber"), "9573259657");
		click(locateElement("xpath","//button[text()='Find Leads']"));
		Thread.sleep(7000);
		String leadId = getText(locateElement("xpath","//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']//a"));
		click(locateElement("xpath","//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']//a"));
		click(locateElement("linkText","Delete"));
		click(locateElement("linkText","Find Leads"));
		type(locateElement("name", "id"), leadId);
		click(locateElement("xpath","//button[text()='Find Leads']"));
		Thread.sleep(7000);
		verifyExactText(locateElement("xpath","//div[@class='x-paging-info']"), "No records to display");
		closeBrowser();	
	}
}

