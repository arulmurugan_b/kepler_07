package week2.day1;

/*import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class LearnListSort implements Collection {

	public static void main(String[] args) {
		
		String str = "Amazon India";
		
		List<Character> ls = new ArrayList<Character>();
		int size = str.length();
		for(int i=0;i<size;i++)
		{
         ls.add(str.charAt(i));			
		}
		System.out.println("The char in list: "+ls);
		
		Collections.sort(ls);
		System.out.println("The char in list: "+ls);
		
*/		
		
		import java.util.ArrayList;
		import java.util.Collections;
		import java.util.List;

		import org.openqa.selenium.WebElement;

		public class LearnListSort {
			
			String txt = "Amazon India";
			
			public void sortChar() {		
				char[] ch = txt.toCharArray();		
				List<Character> lst = new ArrayList<Character>();		
				for (Character eachChar : ch) {
					lst.add(eachChar);
				}				
				//System.out.println(lst);
				Collections.sort(lst);
				System.out.println("Afr sorting "+lst);		
			}
			
			public void reverseChar() {		
				char[] ch = txt.toLowerCase().toCharArray();		
				List<Character> lst = new ArrayList<Character>();		
				for (Character eachChar : ch) {
					lst.add(eachChar);
				}				
				//System.out.println(lst);
				Collections.sort(lst);		
				Collections.reverse(lst);		
				System.out.println("Afr reverse sorting "+lst);		
			}
			
			public void removeDups() {	
				
				char[] ch = txt.toLowerCase().toCharArray();		
				List<Character> lst = new ArrayList<Character>();		
				for (Character eachChar : ch) {
					
					if (!lst.contains(eachChar)) {
						lst.add(eachChar);				
					}			
				}				
				//System.out.println(lst);
				Collections.sort(lst);
				System.out.println("Afr removed dups "+lst);		
			}
			

			public static void main(String[] args) {
				LearnListSort lsort = new LearnListSort();
				lsort.removeDups();
				lsort.sortChar();
				lsort.reverseChar();
			}

		}
		
			

