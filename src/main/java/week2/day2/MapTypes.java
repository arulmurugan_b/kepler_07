package week2.day2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MapTypes {

/*	public static void main(String[] args) {
		String str = "Arul Murugan";
		char[] ch = str.toCharArray();
		Map<String,Long> mp = new HashMap<String, Long>();

		for (char charc : ch) {
			if(mp.containsKey(charc)) {
				Long val = mp.get(charc)+1;
				mp.put(str, val);
				
			}else {
				mp.put(str, 9003790041L);
			}			
		}
			System.out.println(mp);
		}*/
	
public static void main(String[] args) {
		
		String txt = "Amazon India";
		char[] ch = txt.toCharArray();
		
		Map<Character, Integer> map = new HashMap<Character, Integer>();
				
		for (char c : ch) {		
			if (map.containsKey(c)) {				
				Integer val = map.get(c)+1;
				map.put(c, val);				
			}else {
				map.put(c, 1);
			}						
		}
		
		System.out.println(map);
		

}
	
}
