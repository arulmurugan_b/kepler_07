package week2.day2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class SetTypes {

	public static void main(String[] args) {
		
		//Set<String> st = new HashSet<String>();
		Set<String> st = new TreeSet<String>();
		//Set<String> st = new LinkedHashSet<String>();
		st.add("Nokia");
		st.add("Samsung");
		boolean add = st.add("OnePlus");
		System.out.println("Is added once : "+add);
		st.add("Redmi");
		boolean add2 = st.add("OnePlus");
		System.out.println("Is added once : "+add2);
		//System.out.println("The set without a duplicates : "+st);
		
		for (String string : st) {
			System.out.println(string);
			
		}
		List<String> ls = new ArrayList<String>();
		ls.addAll(st);
		String str = ls.get(0);
		System.out.println("The first mobile i purchased: "+str);
		
		
	}
}
