package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethodsNew.ProjectMethods;

public class MyLeads extends ProjectMethods {
	
	public MyLeads() {
	PageFactory.initElements(driver, this);	
	}

@FindBy(how=How.XPATH,using="//a[text()='Create Lead']")
WebElement button4;
public CreateLead myLeads()
{
	//WebElement button2 = locateElement("class", "decorativeSubmit");
	click(button4);
	//MyHome hp = new MyHome();
	return new CreateLead();
}
}
