package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethodsNew.ProjectMethods;

public class LoginPage extends ProjectMethods {
	
	//launch browser
	
	public LoginPage() {
		PageFactory.initElements(driver, this);
		
	}
	@FindBy(id="username")
	WebElement uName;
	public LoginPage typeUserName(String data)
	{
		//WebElement uName = locateElement("id", "username");
		type(uName, data);
		return this;
	}
	@FindBy(id="password")
	WebElement passWord;
	public LoginPage typePassword(String data)
	{
		//WebElement passWord = locateElement("id", "password");
		type(passWord, data);
		return this;
	}
	@FindBy(how=How.CLASS_NAME,using="decorativeSubmit")
	WebElement button1;
	public HomePage loginButton()
	{
		//WebElement button1 = locateElement("class", "decorativeSubmit");
		click(button1);
		//HomePage hp = new HomePage();
		return new HomePage();
	}
}
