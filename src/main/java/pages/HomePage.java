package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethodsNew.ProjectMethods;

public class HomePage extends ProjectMethods {
	
	public HomePage() {
	PageFactory.initElements(driver, this);	
	}

//@FindBy(how=How.XPATH,using="//h2[text()='Demo Sales Manager']")
	WebElement vName;
public HomePage verifyLoggedName(String data) {
	 /*locateElement("xpath", "//h2[text()='Demo Sales Manager']");*/
	
	verifyPartialText(vName, data);
	return this;
		
}
/*
@FindBy(how=How.CLASS_NAME,using="decorativeSubmit")
WebElement button2;
public LoginPage logOutButton()
{
	//WebElement button2 = locateElement("class", "decorativeSubmit");
	click(button2);
	//MyHome hp = new MyHome();
	return new LoginPage();
}
*/
@FindBy(how=How.LINK_TEXT,using="CRM/SFA")
WebElement button2;
public MyHome crmsfaClick() {
	click(button2);
	//MyHome hp = new MyHome();
	return new MyHome();
}

}
