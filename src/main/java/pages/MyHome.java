package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethodsNew.ProjectMethods;

public class MyHome extends ProjectMethods {
	
	public MyHome() {
	PageFactory.initElements(driver, this);	
	}

//@FindBy(how=How.XPATH,using="//h2[text()='Demo Sales Manager']")

public MyHome verifyLoggedPageName(String data1) {
	WebElement vName = locateElement("xpath", "//div[text()='My Home']");
	
	verifyPartialText(vName, data1);
		return this;
}

@FindBy(how=How.XPATH,using="//a[text()='Leads']")
WebElement button3;
public MyLeads clickLeads()
{
	//WebElement button2 = locateElement("class", "decorativeSubmit");
	click(button3);
	//MyHome hp = new MyHome();
	return new MyLeads();
}
}
