package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethodsNew.ProjectMethods;

public class CreateLead extends ProjectMethods {
	
	public CreateLead() {
		PageFactory.initElements(driver, this);	
		}

	@FindBy(id="createLeadForm_companyName")
	WebElement Cname;
	public CreateLead typeCName(String data)
	{
		//WebElement uName = locateElement("id", "username");
		type(Cname, data);
		return this;
	}
	@FindBy(id="createLeadForm_firstName")
	WebElement Fname;
	public CreateLead typeFname(String data)
	{
		//WebElement passWord = locateElement("id", "password");
		type(Fname, data);
		return this;
	}
	@FindBy(id="createLeadForm_lastName")
	WebElement Lname;
	public CreateLead typeLname(String data)
	{
		//WebElement passWord = locateElement("id", "password");
		type(Lname, data);
		return this;
	}
	@FindBy(how=How.CLASS_NAME,using="smallSubmit")
	WebElement button5;
	public CreateLead crLeads()
	{
		//WebElement button2 = locateElement("class", "decorativeSubmit");
		click(button5);
		//MyHome hp = new MyHome();
		//return new FindLead();
		return this;
	}
	
	@FindBy(how=How.XPATH,using="//a[text()='Logout']")
	WebElement button2;
	public LoginPage logOutButton()
	{
		//WebElement button2 = locateElement("class", "decorativeSubmit");
		click(button2);
		//MyHome hp = new MyHome();
		return new LoginPage();
	}
	
	


}
