package wdMethods;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

//import week4.day1.DragAndDrop;

public class SeMethods implements WdMethods{
	public RemoteWebDriver driver;
	public int i = 1;
	public void startApp(String browser, String url) {
		if(browser.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
			driver  = new ChromeDriver();
		} else if(browser.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
			driver  = new FirefoxDriver();
		}
		driver.get(url);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		System.out.println("The Browser "+browser+" launched successfully");
		takeSnap();

	}


	public WebElement locateElement(String locator, String locValue) {
		switch (locator) {
		case "id": return driver.findElementById(locValue);
		case "class": return driver.findElementByClassName(locValue);
		case "xpath": return driver.findElementByXPath(locValue);
		case "linktext": return driver.findElementByLinkText(locValue);
		case "name": return driver.findElementByName(locValue);
		case "partiallinktext": return driver.findElementByPartialLinkText(locValue);
		case "tagname": return driver.findElementByTagName(locValue);
		}
		return null;
	}

	
	public WebElement locateElement(String locValue) {		
		return driver.findElementById(locValue);
	}

	public void type(WebElement ele, String data) {
		ele.sendKeys(data);
		System.out.println("The Data "+data+" is entered successfully");
		takeSnap();
	}


	public void click(WebElement ele) {
	    ele.click();
		System.out.println("The Element "+ele+" is clicked successfully");
		takeSnap();
	}
	public void clickWithoutSnap(WebElement ele) {
		ele.click();
		System.out.println("The Element "+ele+" is clicked without snap successfully");
		
	}
	
	public String getText(WebElement ele) {
		String text = ele.getText();
		return text;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		Select sText = new Select(ele);	
		sText.selectByVisibleText(value);

	}

	@Override
	public void selectDropDownUsingValue(WebElement ele, String value) {
		Select sVal = new Select(ele);	
		sVal.selectByValue(value);

	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		Select sIndex = new Select(ele);
		/*List<WebElement> list = sIndex.getOptions();
		int size = list.size();
		sIndex.selectByIndex(size-2);
*/
		sIndex.selectByIndex(index);

	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
		boolean notPresent = false;
		String title = driver.getTitle();
		if(title.equalsIgnoreCase(expectedTitle))
		{
			return notPresent=true;
		}
		
		return notPresent;
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		String text = ele.getText();
		if(text.equals(expectedText))
		{
			System.out.println("The given text "+text+" is exactly matching..");
		}else {
			System.out.println("The given text "+text+" is NOT exactly matching..");
		}

	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		String text = ele.getText();
		if(text.contains(expectedText)) {
			System.out.println("The given text "+text+" is partially matching..");
		}else {
			System.out.println("The given text "+text+" is NOT partially matching..");
		}

	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		String attr = ele.getAttribute(attribute);
		if(attr.equals(value)) {
			System.out.println("The given attribute "+attr+" is exactly matching..");
		}else {
			System.out.println("The given attribute "+attr+" is NOT exactly matching..");
		}

	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		String attr = ele.getAttribute(attribute);
		if(attr.contains(value)) {
			System.out.println("The given attribute "+attr+" is partially matching..");
		}else {
			System.out.println("The given attribute "+attr+" is NOT partially matching..");
		}


	}

	@Override
	public void verifySelected(WebElement ele) {
		boolean selected1 = ele.isSelected();
		if(selected1) {
			System.out.println("Selected");
		}else {
			System.out.println("Not Selected");
		}

	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		if(ele.isDisplayed()) {
			System.out.println("Displayed");
		}else {
			System.out.println("Not Displayed");
		}
		

	}

	@Override
	public void switchToWindow(int index) {
	
		Set<String> winHandles = driver.getWindowHandles();
		List<String> ls = new ArrayList<String>();
		ls.addAll(winHandles);
		driver.switchTo().window(ls.get(index));	
	}

	@Override
	public void switchToFrame(WebElement ele) {
		driver.switchTo().frame(ele);

	}

	@Override
	public void acceptAlert() {
		driver.switchTo().alert().accept();

	}

	@Override
	public void dismissAlert() {
		driver.switchTo().alert().dismiss();

	}

	@Override
	public String getAlertText() {
		String text = driver.switchTo().alert().getText();
		return text;
	}


	public void takeSnap() {
		try {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File desc = new File("./snaps/img"+i+".jpeg");		
		FileUtils.copyFile(src, desc);
		} catch (IOException e) {
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void closeBrowser() {
		String wHandle = driver.getWindowHandle();
		driver.switchTo().window(wHandle).close();

	}

	@Override
	public void closeAllBrowsers() {
		/*Set<String> allWhandles = driver.getWindowHandles();
		List<String> ls = new ArrayList<String>();
		ls.addAll(allWhandles);*/
		driver.close();
	}

}
