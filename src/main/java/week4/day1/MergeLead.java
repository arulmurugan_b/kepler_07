package week4.day1;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class MergeLead {

	public static void main(String[] args) {

		//to access chrome browser
System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
ChromeDriver driver = new ChromeDriver();
//to maximize
driver.manage().window().maximize();
//set timeout
driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//to enter url
driver.get("http://leaftaps.com/opentaps/");
//enter username
driver.findElementById("username").sendKeys("DemoSalesManager");
//enter password
driver.findElementById("password").sendKeys("crmsfa");
//click login
driver.findElementByClassName("decorativeSubmit").click();
//click links
driver.findElementByLinkText("CRM/SFA").click();
driver.findElementByLinkText("Create Lead").click();
//enter companyname
driver.findElementById("createLeadForm_companyName").sendKeys("Solartis Tech");
//enter firstname
driver.findElementById("createLeadForm_firstName").sendKeys("Arul");
//enter lastname
driver.findElementById("createLeadForm_lastName").sendKeys("Murugan");

//using visibleText
WebElement src = driver.findElementById("createLeadForm_dataSourceId");
Select dropDown = new Select(src);
//dropDown.selectByVisibleText("Direct Mail");
dropDown.selectByValue("LEAD_CONFERENCE");


//using index with list
WebElement src1 = driver.findElementById("createLeadForm_marketingCampaignId");
Select dropDown1 = new Select(src1);
List<WebElement> list = dropDown1.getOptions();
int size = list.size();
dropDown1.selectByIndex(size-2);

//click the save button
driver.findElementByClassName("smallSubmit").click();

/*
(//img[contains(@src, 'images/fieldlookup.gif')])[1]

*/	}

}
