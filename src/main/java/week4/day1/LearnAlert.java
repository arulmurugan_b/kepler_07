package week4.day1;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class LearnAlert {

	public static void main(String[] args) {

		//to access chrome browser
System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
ChromeDriver driver1 = new ChromeDriver();
//to maximize
driver1.manage().window().maximize();
//set timeout
driver1.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//to enter url
driver1.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
//button[text()='Try it']
driver1.switchTo().frame("iframeResult");
driver1.findElementByXPath("//button[text()='Try it']").click();
driver1.switchTo().alert().sendKeys("Arul");
driver1.switchTo().alert().accept();
/*String text = driver1.switchTo().alert().getText();
System.out.println(text);
driver1.switchTo().alert().accept();
*/
	
	}

}
