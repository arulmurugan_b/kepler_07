package week4.day1;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class LearnKeyMouseActions {

	public static void main(String[] args) throws IOException {

		//to access chrome browser
System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
ChromeDriver driver1 = new ChromeDriver();
//to maximize
driver1.manage().window().maximize();
//set timeout
driver1.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//to enter url
driver1.get("http://jqueryui.com/draggable/");

Actions act = new Actions(driver1);
WebElement frame1 = driver1.findElementByClassName("demo-frame");
WebDriver frame = driver1.switchTo().frame(frame1);

act.dragAndDropBy(frame1, 50, 30);

	
	}

}
