package week4.day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.etsi.uri.x01903.v13.impl.GenericTimeStampTypeImpl;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class LearnSwitchWindow {

	public static void main(String[] args) throws IOException {

		//to access chrome browser
System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
ChromeDriver driver1 = new ChromeDriver();
//to maximize
driver1.manage().window().maximize();
//set timeout
driver1.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//to enter url
driver1.get("https://www.irctc.co.in/");
driver1.findElementByXPath("//span[text()='AGENT LOGIN']").click();
driver1.findElementByXPath("//a[text()='Contact Us']").click();
Set<String> winHan = driver1.getWindowHandles();
List<String> lsWin = new ArrayList<>();
lsWin.addAll(winHan);
System.out.println(driver1.getCurrentUrl());
System.out.println(driver1.getTitle());

driver1.switchTo().window(lsWin.get(1));
winHan = driver1.getWindowHandles();
lsWin = new ArrayList<>();
lsWin.addAll(winHan);
System.out.println(driver1.getCurrentUrl());
System.out.println(driver1.getTitle());

File screen = driver1.getScreenshotAs(OutputType.FILE);
File desc = new File("./snapshot/image.png");
FileUtils.copyFile(screen, desc);
driver1.switchTo().window(lsWin.get(0)).close();


	
	}

}
