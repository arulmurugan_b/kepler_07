package week1.day2;

import java.util.Scanner;

public class LearnStringFunc {

	public static void main(String[] args) {
		 
		//Scanner scan = new Scanner(System.in);
		//String text = scan.nextLine();
		

		//Simplest way to create a string 
/*String text = "amazon";
System.out.println(text.length());

char[] textCh = text.toCharArray();
System.out.println("Using toCharArray: "+textCh.length);
*/		
      
		//Another way to create a string

/*char[] textCh = {'a','m','a','z','o','n'};
String newString = new String(textCh);
*/
		
		
//charAt -to get last char of string
		/*String text = "amazon";
System.out.println("The last char of string: "+text.charAt(text.length()-1));
*/
		
//Concat string

		/*String text = "amazon";
		System.out.println("After concat of string: "+text.concat(" india"));		
*/

//sub-string
		/*String text = "hexawaretype";
		System.out.println("The substring :" +text.substring(0, 8));
*/

//split

/*String name = "sarath,gopi,babu";
		String[] nameA = name.split(",");
		
		for(String nameB : nameA) {
			System.out.println(nameB);
*/
		

	
	} 

	}


