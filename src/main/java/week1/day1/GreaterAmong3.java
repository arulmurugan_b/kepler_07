package week1.day1;

import java.util.Scanner;

public class GreaterAmong3 {

	//Print greatest among the three number
	
	public static void main(String[] args) {
		//creating object
		Scanner scr = new Scanner(System.in);
		System.out.println("Enter the number");
		//calling function 
		int num1 = scr.nextInt();
		int num2 =scr.nextInt();
		int num3 =scr.nextInt();
		
		if(num1>num2&&num1>num3) {
			System.out.println("Num1 is greater "+num1);
		}
		else if (num2>num3) {
			System.out.println("Num2 is greater "+num2);
		}
		else {
			System.out.println("Num3 is greater "+num3);
		}

	}

}
