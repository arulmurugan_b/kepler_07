package week1.day1;

import java.util.Scanner;

public class SwitchCase {

	public static void main(String[] args) {
		// using switch case
		
		Scanner scr = new Scanner(System.in);
		System.out.println("Enter the number1: ");
		int num1 = scr.nextInt();
		System.out.println("Enter the number2: ");
		int num2 = scr.nextInt();
		System.out.println("Enter the Choice: ");

		int option = scr.nextInt();
		
		switch(option) {
		case 1:
			System.out.println("Addition: "+(num1+num2));
			break;
			//to stop the case one
		case 2:
			System.out.println("Subtraction: "+(num1-num2));
			break;
			
			default:
				System.out.println("Invalid choice");
		}

	}

}
