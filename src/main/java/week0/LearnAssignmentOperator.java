package week0;

import java.util.Scanner;

public class LearnAssignmentOperator {

	public static void main(String[] args) {
		// For assignment & shorthand operation of two numbers
				
		System.out.println("Enter the number ");
		Scanner scan = new Scanner(System.in);
		int n1 = scan.nextInt();
		int n2 = scan.nextInt();
		int n3 = scan.nextInt();
		int n4 = scan.nextInt();
		int n5 = scan.nextInt();
		int n6 = scan.nextInt();
		int n7 = scan.nextInt();
		int n8 = scan.nextInt();
		int n9 = scan.nextInt();
		int n10 = scan.nextInt();
		
		//addition of (shorthand op)
		System.out.println("num1 adding num2 = "+(n1+=n2));
		//subtract of (shorthand op)
		System.out.println("num1 subtracts num2 = "+(n3 -= n4));
		//multiple of (shorthand op)
		System.out.println("num1 multiplies num2 = "+(n5*=n6));
		//divide of (shorthand op)
		System.out.println("num1 divides num2 = "+(n7/=n8));
		// modulus of (shorthand op)
		System.out.println("num1 is modulus of num2 = "+(n9%=n10));
		
	}

}
