package week0;

public class LearnDatatype {

	public static void main(String[] args) {
		
		int a = 2147483647;
		boolean b = true;
		long c= 922337277456222545L;
		short d = 32767;
		double e = 455.4545578d;
		float f = 4522455.499f;
		char g = 'a';
		byte h = 127;
		//Print the integer 32bit
		System.out.println("The integer value is " +a);
		//Print the boolean 1bit consider
		System.out.println("The boolean is " +b);
		//Print the long 64bit
		System.out.println("The long is "+c);
		//Print the short 16bit
		System.out.println("The short is "+d);
		//Print the double 64bit
		System.out.println("The double is "+e);
		//Print the float 32bit
		System.out.println("The float is "+f);
		//Print the char 16bit unicode
		System.out.println("The character is "+g);
		//Print the byte 8bit
		System.out.println("The byte is "+h);

	}

}
