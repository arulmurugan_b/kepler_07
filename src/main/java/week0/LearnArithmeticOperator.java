package week0;

import java.util.Scanner;

public class LearnArithmeticOperator {

	public static void main(String[] args) {
		// For arithmetic operation of two numbers
				
		System.out.println("Enter the number ");
		Scanner scan = new Scanner(System.in);
		int n1 = scan.nextInt();
		int n2 = scan.nextInt();
		
		//addition
		System.out.println("addition of two numbers = "+(n1+n2));
		//subtract
		System.out.println("subtraction of two numbers = "+(n1-n2));
		//multiply
		System.out.println("multiple of two numbers = "+(n1*n2));
		//divide
		System.out.println("division of two numbers = "+n1/n2);

		

	}

}
