 package week0;

import java.util.Scanner;

public class LearnRelationalOperator {

	public static void main(String[] args) {
		// For relational operation of two numbers
				
		System.out.println("Enter the number ");
		Scanner scan = new Scanner(System.in);
		int n1 = scan.nextInt();
		int n2 = scan.nextInt();
		
		//greater than
		System.out.println("num1 greater than num2 = "+(n1>n2));
		//lesser than
		System.out.println("num1 lesser than num2 = "+(n1<n2));
		//greater than or equal to
		System.out.println("num1 greater or equal than num2 = "+(n1>=n2));
		//lesser than or equal to
		System.out.println("num1 lesser or equal than num2 = "+(n1<=n2));
		// equal to
		System.out.println("num1 is equal to num2 = "+(n1==n2));
		//not equal to
		System.out.println("num1 not equal to num2 = "+(n1!=n2));	
		//modulus
		System.out.println("num1 is modulus of num2 = "+ (n1%n2));

	}

}
