package week0;

import java.util.Scanner;

public class LearnEvenOdd {

	public static void main(String[] args) {
		// Print the given no is even or odd
				
		System.out.println("Enter the number ");
		Scanner scan = new Scanner(System.in);
		int n1 = scan.nextInt();
		
		if(n1%2==0)
		{
			System.out.println("The given no is Even");
		}
		else
			System.out.println("The given no is Odd");

	}

}
