package week3.day2;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class LearnErailTrains {

	public static void main(String[] args) {

		//to access chrome browser
System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
ChromeDriver driver1 = new ChromeDriver();
//to maximize
driver1.manage().window().maximize();
/*//set timeout
driver1.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);*/
//to enter url
driver1.get("http://www.leafground.com/pages/table.html");

List<WebElement> allCheckBox = driver1.findElementsByXPath("//input[@type='checkbox']");
int size = allCheckBox.size();
System.out.println(size);
allCheckBox.get(size-1).click();

WebElement check80percent = driver1.findElementByXPath("//font[text()[contains(.,'80')]]/following::input");
check80percent.click();
	}

}
