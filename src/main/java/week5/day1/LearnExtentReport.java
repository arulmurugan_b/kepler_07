package week5.day1;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnExtentReport {
	
	public static void main(String[] args) throws IOException {
	
		ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/result.html");
		html.setAppendExisting(false);
		ExtentReports ext = new ExtentReports();
		ext.attachReporter(html);
		//testcase level
		ExtentTest cTest = ext.createTest("TC_0001", "Create a new Lead");
		cTest.assignAuthor("Arul");
		cTest.assignCategory("SeleniumAutomation");
		//testcase step level
		//add the existing image to Extent Report
		cTest.pass("The browser launch successfully", 
				MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
		cTest.fail("The data DemoSalesManager not entered successfully", 
				MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img2.png").build());
		cTest.pass("The data crmsfa entered successfully", 
				MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img3.png").build());
		
		ext.flush();
		
		
	}
	

}
